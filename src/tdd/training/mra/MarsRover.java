package tdd.training.mra;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private Set<String> obstacles;
	protected int roverX;
	protected int roverY;
	protected enum Direction {N, S, E, W}
	protected Direction direction;
	protected String status;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.roverX = 0;
		this.roverY = 0;
		this.direction = Direction.N;
		this.status = "(0,1,N)";
		this.obstacles = new LinkedHashSet<String>();
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean check = false;
		
		if(x < 0 || y < 0) {
			throw new MarsRoverException("Coordinates cannot be less than 0");
		} 
		else {
			String coordinate = "(" + x + "," + y + ")";
			
			for(int i = 0; i < this.planetObstacles.size(); i++) {
				if(this.planetObstacles.get(i).equals(coordinate))
					check = true;
			}
		}
		return check;	
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString.equals("")) 
			return this.status = ("(0,0,N)");
		else {
			for(int i = 0; i < commandString.length(); i++) {
				this.status = checkCommand(commandString.substring(i, i+1));
			}
			for(String obstacle: this.obstacles)
				this.status = this.status + obstacle;
			
			return this.status;
		}
	}
	
	private String checkCommand(String commandString) throws MarsRoverException {
		
		if(commandString.equals("r")) 
			return turnRight();
		else if(commandString.equals("l"))
			return turnLeft();
		else if(commandString.equals("f"))
			return movingForward();
		else if(commandString.equals("b"))
			return movingBackward();
		else
			throw new MarsRoverException("Invalid command received.");
	}
	
	
	private void setStatus() {
		this.status = "(" + this.roverX + "," + this.roverY + "," + this.direction + ")";
	}
	
	private String turnRight() {
		switch(this.direction) {
		case N:
			this.direction = Direction.E;
			setStatus();
			break;
		case E:
			this.direction = Direction.S;
			setStatus();
			break;
		case S:
			this.direction = Direction.W;
			setStatus();
			break;
		case W:
			this.direction = Direction.N;
			setStatus();
			break;
		}
		
		return this.status;
	}
	
	private String turnLeft() {
		switch(this.direction) {
		case N:
			this.direction = Direction.W;
			setStatus();
			break;
		case E:
			this.direction = Direction.N;
			setStatus();
			break;
		case S:
			this.direction = Direction.E;
			setStatus();
			break;
		case W:
			this.direction = Direction.S;
			setStatus();
			break;
		}
		
		return this.status;
	}
	
	private String movingForward() throws MarsRoverException {
		switch(this.direction) {
		case N:
			if(this.roverY == this.planetY-1) {
				if(!planetContainsObstacleAt(this.roverX, 0))
					this.roverY = 0;
				else
					this.obstacles.add("(" + this.roverX + ",0)");
			}	
			else {
				if(!planetContainsObstacleAt(this.roverX, this.roverY+1))
					this.roverY++;
				else {
					int y = this.roverY + 1;
					this.obstacles.add("(" + this.roverX + "," + y + ")");
				}
			}
			setStatus();
			break;
		case E:
			if(this.roverX == this.planetX-1) {
				if(!planetContainsObstacleAt(0, this.roverY))
					this.roverX = 0;
				else
					this.obstacles.add("(0," + this.roverY + ")");
			}
			else {
				if(!planetContainsObstacleAt(this.roverX+1, this.roverY))
					this.roverX++;
				else {
					int x = this.roverX + 1;
					this.obstacles.add("(" + x + "," + this.roverY + ")");
				}
			}
			setStatus();
			break;
		case S:
			if(this.roverY == 0) {
				if(!planetContainsObstacleAt(this.roverX, this.planetY-1))
					this.roverY = this.planetY-1;
				else {
					int y = this.planetY - 1;
					this.obstacles.add("(" + this.roverX + "," + y + ")");
				}
			}	
			else {
				if(!planetContainsObstacleAt(this.roverX, this.roverY-1))
					this.roverY--;
				else {
					int y = this.roverY - 1;
					this.obstacles.add("(" + this.roverX + "," + y + ")");
				}
			}
			setStatus();
			break;
		case W:
			if(this.roverX == 0) {
				if(!planetContainsObstacleAt(this.planetX-1, this.roverY))
					this.roverX = this.planetX-1;
				else {
					int x  = this.planetX - 1;
					this.obstacles.add("(" + x + "," + this.roverY + ")");
				}
			}
			else {
				if(!planetContainsObstacleAt(this.roverX-1, this.roverY))
					this.roverX--;
				else {
					int x  = this.roverX - 1;
					this.obstacles.add("(" + x + "," + this.roverY + ")");
				}
			}
			setStatus();
			break;
		}
		return this.status;
	}
	
	
	private String movingBackward() throws MarsRoverException {
		switch(this.direction) {
			case N:
				if(this.roverY == 0) {
					if(!planetContainsObstacleAt(this.roverX, this.planetY-1))
						this.roverY = this.planetY-1;
					else {
						int y = this.planetY - 1;
						this.obstacles.add("(" + this.roverX + "," + y + ")");
					}
				}
				else {
					if(!planetContainsObstacleAt(this.roverX, this.roverY-1))
						this.roverY--;
					else {
						int y = this.roverY - 1;
						this.obstacles.add("(" + this.roverX + "," + y + ")");
					}
				}
				setStatus();
				break;
			case E:
				if(this.roverX == 0) {
					if(!planetContainsObstacleAt(this.planetX-1, this.roverY))
						this.roverX = this.planetX-1;
					else {
						int x = this.planetX - 1;
						this.obstacles.add("(" + x + "," + this.roverY + ")");
					}
				}
				else {
					if(!planetContainsObstacleAt(this.roverX-1, this.roverY))
						this.roverX--;
					else {
						int x = this.roverX - 1;
						this.obstacles.add("(" + x + "," + this.roverY + ")");
					}
				}
				setStatus();
				break;
			case S:
				if(this.roverY == this.planetY-1) {
					if(!planetContainsObstacleAt(this.roverX, 0))
						this.roverY = 0;
					else
						this.obstacles.add("(" + this.roverX + ",0)");
				}
				else {
					if(!planetContainsObstacleAt(this.roverX, this.roverY+1))
						this.roverY++;
					else {
						int y = this.roverY + 1;
						this.obstacles.add("(" + this.roverX + "," + y + ")");
					}
				}	
				setStatus();
				break;
			case W:
				if(this.roverX == this.planetX-1) {
					if(!planetContainsObstacleAt(0, this.roverY))
						this.roverX = 0;
					else
						this.obstacles.add("(0," + this.roverY + ")");
				}
				else {
					if(!planetContainsObstacleAt(this.roverX + 1, this.roverY))
						this.roverX++;
					else {
						int x = this.roverX + 1;
						this.obstacles.add("(" + x + "," + this.roverY + ")");
					}
				}
				setStatus();
				break;
			}
		return this.status;
	}

}